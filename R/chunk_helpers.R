#' Label background chunks
#'
#' @param dst_path `chr` -- absolute path to the folder where label tracks of
#' background are saved chunks
#' @param origin `chr` -- audio file name without extension
#' @param min_start `number` (in s) -- the minimum authorized start (due to
#' normalisation)
#' @return `num` -- number of background chunks labeled
#' @export
#'
#'
#' @keywords internal
#'
chunk_background <- function(dst_path, origin, min_start) {
  more_bg <- TRUE
  bg_label_track <- data.frame(matrix(ncol = 5, nrow = 0))

  i <- 0
  while (more_bg) {
    start <- prompt_num(glue("Enter start time of background chunk (in s), ",
                             "[Enter] when done: "),
                        empty_allowed = TRUE
    )
    if (is.null(start)) {
      more_bg <- FALSE
    } else {
      if (start < min_start) {
        print(glue("You chose to force normalisation.
          A sound of duration {round(min_start, digits=2)}s was added at the ",
                   "beginning of the sound.
          The chunk of background will thus be labeled from ",
                   "{round(min_start, digits=2)}s instead of {start}s."))
        start <- min_start
      }
      i <- i + 1
      dur <- prompt_num("Enter duration of background chunk (in s): ")
      end <- start + dur
      bg_label_track <- rbind(bg_label_track, c(start, end, "bg", i, origin))
    }
  }

  columns <- c("start", "end", "label", "index", "origin")
  colnames(bg_label_track) <- columns

  ## export label track in dst_path
  label_track_name <- glue("{origin}.bg.txt")
  label_track_file <- file.path(dst_path, label_track_name)
  write.table(bg_label_track, label_track_file,
              sep = "\t", row.names = FALSE, quote = FALSE
  )

  print(glue(
    "{i} chunks of background noise saved in label track: {label_track_name}
    exported in {dst_path}"
  ))

  n_bg_chunks <- i
  return(n_bg_chunks)
}

#' Tune settings
#'
#' @description
#' Allows to change several settings for detection until the optimized settings
#' are reached
#'
#' @inheritParams detection_parameters_doc
#'
#' @param sound_filtered -- a wav object
#' @param origin `chr`-- name of the file with no extension
#' @param samp_rate `num` -- frequency sampling of the wav object (Hz)
#' @param default_label `chr` -- character string of one element. Default label
#' displayed in detected chunks
#' @param norm_sound_dur `num` (in s) -- duration of the synthetic sound that
#' has been added to ensure normalisation
#'
#' @return A list of three objects: the last `timer` plot, the table of
#' detections, the final detection settings
#'
#' @export
#'
tune_settings <- function(sound_filtered, origin, threshold, power,
                          env_window_length, env_window_ov,
                          min_dur, samp_rate, default_label,
                          norm_sound_dur) {
  good_settings <- FALSE
  while (!good_settings) {
    print(glue("
      Possible settings:
        power..............[p]
        threshold..........[t]
        min_dur............[d]
        env_window_length..[l]
        env_window_ov......[o]
        listen to audio....[a]
         "))

    set <- prompt_acceptable(
      "Choose a setting or press [enter] if done: ",
      c("p", "t", "d", "l", "o", "a", "")
    )
    if (set == "") {
      break
    } else if (set == "p") {
      power <- prompt_num(glue("New power argument (current: {power}): "))
    } else if (set == "t") {
      threshold <- prompt_num(glue(
        "New threshold argument ",
        "(current: {threshold}): "
      ))
    } else if (set == "d") {
      min_dur <- prompt_num(glue(
        "New min_dur argument ",
        "(current: {min_dur}s): "
      ))
    } else if (set == "l") {
      env_window_length <- prompt_num(glue(
        "New env_window_length argument ",
        "(current: {env_window_length}pts) : "
      ))
    } else if (set == "o") {
      env_window_ov <- prompt_num(glue(
        "New env_window_ov argument ",
        "(current: {env_window_ov}%) : "
      ))
    } else if (set == "a") {
      ## option listen to the sound
      if (!is.null(get_audio_player_path())) {
        listen(sound_filtered, samp_rate)
      } else {
        catn("Path to an audio player for is not defined.")
        catn("Consider using set_audio_player_path to enable listening to sounds.")
      }
    }

    screen(2, TRUE)
    plot_main <- glue(
      "File {origin}:\npower={power}, min_dur={min_dur}, ",
      "env_wdw_len={env_window_length}, env_wdw_ov={env_window_ov}"
    )
    error_param <- ""
    tryCatch(
      {
        sound_detect <- timer(sound_filtered,
                              msmooth = c(env_window_length, env_window_ov),
                              power = power, threshold = threshold,
                              dmin = min_dur, plot = TRUE, f = samp_rate,
                              main = plot_main
        )
      },
      error = function(cond) {
        error_param <<- "No detection by timer, no plot generated"
      }
    )

    ## check that timer can still work with this set of settings
    if (nchar(error_param) > 0) {
      print(error_param)
      detect <- data.frame(matrix(ncol = 4, nrow = 0))
      colnames(detect) <- c("start", "end", "label", "index")
    } else {
      sound_detect <- clean_timer_output(sound_detect, norm_sound_dur, min_dur)

      start <- sound_detect$s.start
      end <- sound_detect$s.end
      detect <- data.frame(start, end)
      if (nrow(detect) > 0) {
        detect["label"] <- default_label
        detect["index"] <- seq_len(nrow(detect))
      }
    }
  }
  ## good settings so plot labels
  if (nrow(detect) > 0) {
    ## record plot no label with final settings
    plot_no_label <- recordPlot()

    ## show labels
    seg <- norm_sound_dur + detect$start + (0.5 * (detect$end - detect$start))
    text(seg, 0.9, detect$index, col = "magenta", cex = 1)
    text(seg, 0.60, detect$label, col = "magenta", cex = 1)
  } else {
    plot_no_label <- NULL
  }

  ## final settings
  final_settings <- data.frame(
    threshold, power, env_window_length,
    env_window_ov, min_dur
  )

  return(list(
    plot_no_label = plot_no_label, detect = detect,
    final_settings = final_settings
  ))
}


#' Semi Manual Labeling
#'
#' @description
#' Allow manual editing of labels in an initial label track `detect` using a
#' set of `accepted labels` and export final label track in `dst_path`
#'
#' @details
#' labels t are used for trashing false positive
#'
#' @param plot_no_label -- a timer plot recorded
#' @param detect `dataframe` -- with start, end, label columns to display the
#' detected and labeled chunks
#' @param accepted_labels -- a vector of string containing the possible labels.
#' By default, the first element of the vector is considered the `default_label`
#' and the `t` label is added to discard false positive
#' @param origin `chr` -- file name without extension
#' @param dst_path `chr` --  to where the label tracks will be saved
#' @param norm_sound_dur (s) -- duration of normalisation sound
#'
#'
#' @return `dataframe` -- with the detection (unfiltered, i.e. including trash
#'                        labels)
#'
#' @export
#'
semi_manual_labeling <- function(plot_no_label, detect, accepted_labels,
                                 origin, dst_path, norm_sound_dur) {
  edit <- TRUE
  default_label <- accepted_labels[1]

  if (is.null(plot_no_label)) {
    catn("No chunks detected in file. Back to main menu")
    return(data.frame())
  }

  if (nrow(detect) == 0) {
    catn("No chunks detected in file. Back to main menu")
    return(detect)
  }

  replayPlot(plot_no_label)
  plot_detect_labels(detect, norm_sound_dur)

  ## edit labels when user wants to
  while (edit) {
    index <- prompt_num("Enter the index to correct, press [enter] if done: ",
                        empty_allowed = TRUE
    )
    if (is.null(index)) {
      break
    }
    if (index > nrow(detect)) {
      print(glue("You must choose a label between 1 and {nrow(detect)}"))
      index <- prompt_num("Enter the index to correct, press [enter] if done: ",
                          empty_allowed = TRUE
      )
    }

    new_label <- prompt_acceptable(
      "Enter the new label (by default, [t] for 'trash'): ",
      unique(c(default_label, accepted_labels, "t"))
    )
    detect$label[index] <- new_label

    replayPlot(plot_no_label)
    plot_detect_labels(detect, norm_sound_dur)
  }

  ## clean up the dataframe of final good chunks
  detect_selected <- detect[detect$label != "t", ]
  if (show_steps()) {
    print(glue("{nrow(detect_selected)} good chunks after removing ",
               "trash labels [t]"))
    print("Label track table: ")
    print(detect_selected)
    pause()
  }

  if (nrow(detect_selected) > 0) {
    detect_selected["origin"] <- origin
    ## export final label track
    label_track_name <- glue("{origin}.{default_label}.labels.txt")
    label_track_file <- file.path(dst_path, label_track_name)

    write.table(detect_selected, label_track_file,
                sep = "\t", row.names = FALSE, quote = FALSE
    )
    print(glue(
      "Clean label track file {label_track_name} saved in {dst_path}"
    ))
  } else {
    print(glue(
      "No clean chunks in file {origin}. No label track exported.'"
    ))
  }

  if (show_steps()) {
    print(glue("Showsteps On!
                   Label track of this file looks like that"))
    print(detect_selected)
    pause()
  }

  return(detect)
}


#' Ask for quality of detection (false/true/positive/negative)
#'
#' @param n_chunks `numerical` -- number of detected chunks
#'
#' @return one-line `dataframe`
#' @export
#'
#' @keywords internal
#'
chunk_count <- function(n_chunks) {
  ## count the number of missed chunks
  false_negative <- prompt_num("Number of missed chunks: ")

  ## count the number of false positive --> t or T labels
  false_positive <- prompt_num("Number of false positive chunks: ")

  ## count number of good chunks
  n_good_chunks <- prompt_num("Number of good chunks: ")
  while (n_good_chunks < 0 || n_good_chunks > n_chunks) {
    cat(glue("There are {n_chunks} chunks in total"))
    n_good_chunks <- prompt_num("Number of good chunks: ")
  }

  ## sensitivity
  sensitivity <- n_good_chunks / (n_good_chunks + false_negative) * 100

  return(data.frame(false_negative, false_positive, n_good_chunks, n_chunks,
                    sensitivity))
}



#' Clean timer output
#'
#'
#'
#' @param sound_detect -- output of timer
#' @param norm_sound_dur (s) -- duration of normalisation sound
#' @param min_dur (s) -- minimum duration for chunk detection
#'
#' @return -- clean sound_detect object
#' @export
#'
#' @seealso [timer()]
clean_timer_output <- function(sound_detect, norm_sound_dur, min_dur) {

  ## before: time referential: 0 when synthetic starts
  sound_detect$s.start <- sound_detect$s.start - norm_sound_dur
  sound_detect$s.end <- sound_detect$s.end - norm_sound_dur
  ## after: time referential: 0 when actual signal slice starts

  # print(sound_detect)

  ## exclude when end before actual signal
  has_too_early_ends <- which(sound_detect$s.end <= 0)
  if (length(has_too_early_ends) > 0) {
    sound_detect$s.start <- sound_detect$s.start[-has_too_early_ends]
    sound_detect$s <- sound_detect$s[-has_too_early_ends]
    sound_detect$s.end <- sound_detect$s.end[-has_too_early_ends]
  }

  ## push forward start to 0 when starting before actual signal
  starting_before <- which(sound_detect$s.start < 0)
  for (idetect in seq_len(length(starting_before))) {
    sound_detect$s.start[idetect] <- 0
    sound_detect$s[idetect] <- sound_detect$s.end[idetect] - sound_detect$s.start[idetect]
  }

  ## reapply minimum duration constraint
  too_short <- which(sound_detect$s < min_dur)
  if (length(too_short) > 0) {
    sound_detect$s.start <- sound_detect$s.start[-too_short]
    sound_detect$s <- sound_detect$s[-too_short]
    sound_detect$s.end <- sound_detect$s.end[-too_short]
  }

  return(sound_detect)
}


#' Plot label detection
#'
#' @param detect `dataframe` -- columns `start` and `end`: result of detection
#' @param norm_sound_dur `num` (in s) -- the duration of the synthetic sound
#'                       that has been added for normalisation
#'
#' @export
#'
#' @keywords internal
plot_detect_labels <- function(detect, norm_sound_dur) {
  index_y <- 0.9
  label_y <- 0.70
  if (norm_sound_dur > 0) {
    seg <- c(norm_sound_dur/2,
             norm_sound_dur + detect$start + (0.5 * (detect$end - detect$start)))
    text(seg, index_y, c(0, detect$index), col = "purple", cex = 1)
    text(seg, label_y, c('t', detect$label), col = "purple", cex = 1)
  } else {
    seg <- detect$start + (0.5 * (detect$end - detect$start))
    text(seg, index_y, detect$index, col = "purple", cex = 1)
    text(seg, label_y, detect$label, col = "purple", cex = 1)
  }
}
